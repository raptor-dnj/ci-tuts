<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 6/2/2016
 * Time: 9:01 PM
 */


class Home extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model("My_model");
    }


    function index(){

       $datav['sum'] =  $this->My_model->add(array(10,20,30,40,50,60));
        $this->load->view("my_view",$datav);
    }

    function shubho(){

        $datav['persons'] =  $this->My_model->getAllPerson();
        $this->load->view('person', $datav);
    }

}