<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Title of the document</title>
</head>

<body>
<h2>Person List</h2>
<table>
    <tr><th></th><th></th><th></th></tr>
<?php
$i = 1;
foreach($persons  as $person): ?>
<tr><td><?php echo $i; ?></td><td><?php echo $person['person_name']; ?></td><td><?php echo $person['person_email']; ?></td></tr>

<?php
$i = $i +1;
endforeach;?>
</table>
</body>

</html>